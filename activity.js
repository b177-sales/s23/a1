// 3
db.users.insertOne({
	roomName: "single",
	accomodates: 2,
	roomPrice: 1000,
	roomDescription: "A simple room with all the basic necessities",
	roomsAvailable: 10,
	isAvailable: false
})

// 4
db.users.insertMany([
	{
		roomName: "double",
		accomodates: 3,
		price: 2000,
		roomDescription: "A room fit for a small family going on vacation",
		roomsAvailable: 5,
		isAvailable: false
	},
	{
		roomName: "double1",
		accomodates: 3,
		price: 2000,
		roomDescription: "A room fit for a small family going on vacation",
		roomsAvailable: 5,
		isAvailable: false
	},
	{
		roomName: "queen",
		accomodates: 4,
		price: 4000,
		roomDescription: "A room with a queen sized bed perfect for a simple getaway",
		roomsAvailable: 15,
		isAvailable: false
	},
	{
		roomName: "queen1",
		accomodates: 4,
		price: 4000,
		roomDescription: "A room with a queen sized bed perfect for a simple getaway",
		roomsAvailable: 15,
		isAvailable: false
	}
])

// 5
db.users.find({roomName: "double"})

// 6
db.users.updateOne(
	{_id: ObjectId("62877fa70fbb04b5ce34dd17")},
	{
		$set: {
			roomsAvailable: 0
		}
	}
)

// 7
db.users.deleteMany({
	roomsAvailable: 0
})





